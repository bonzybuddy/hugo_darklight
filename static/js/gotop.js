const CLASS_NAME_GO_SHOW = "show";
const goTop = document.querySelector(".go-top");

window.addEventListener("scroll", function () {
    const scrollHeight = window.scrollY;
    if (scrollHeight > 30) {
        goTop.classList.add(CLASS_NAME_GO_SHOW);
    } else {
        goTop.classList.remove(CLASS_NAME_GO_SHOW);
    }
});