const CLASS_NAME_LIGHT = 'ilone-light';
const CLASS_NAME_DARK = 'ilone-dark';
const CLASS_NAME_NAVBAR = 'navbar';
const CLASS_NAME_NAVBAR_DARK = 'navbar-dark';
const CLASS_NAME_NAVBAR_LIGHT = 'navbar-light';
const STORAGE_LIGHT_ITEM = 'isLight';

document.addEventListener('DOMContentLoaded', function () {
  const lightswitch = document.querySelector('.lightswitch');
  let isLight = localStorage.getItem(STORAGE_LIGHT_ITEM);

  if (isLight === null) {
    isLight =
      window.matchMedia &&
      window.matchMedia('(prefers-color-scheme: light)').matches;
  } else {
    isLight = isLight === 'true';
  }

  updateAll(isLight, lightswitch);

  lightswitch.addEventListener('click', function (e) {
    const light = document.body.classList.contains(CLASS_NAME_LIGHT);
    updateAll(!light, this);
  });
});

const updateAll = (light = false, lightswitch) => {
  const LIGHT_ICON = lightswitch.getAttribute('light-icon');
  const DARK_ICON = lightswitch.getAttribute('dark-icon');

  updateTheme(light);
  if (light) {
    lightswitch.innerHTML = LIGHT_ICON;
  } else {
    lightswitch.innerHTML = DARK_ICON;
  }
};

const updateTheme = (light = false) => {
  const navbar = document.querySelector(`.${CLASS_NAME_NAVBAR}`);
  if (light) {
    document.body.classList.remove(CLASS_NAME_DARK);
    document.body.classList.add(CLASS_NAME_LIGHT);
    localStorage.setItem(STORAGE_LIGHT_ITEM, 'true');
    navbar.classList.remove(CLASS_NAME_NAVBAR_DARK);
    navbar.classList.add(CLASS_NAME_NAVBAR_LIGHT);
  } else {
    document.body.classList.remove(CLASS_NAME_LIGHT);
    document.body.classList.add(CLASS_NAME_DARK);
    localStorage.setItem(STORAGE_LIGHT_ITEM, 'false');
    navbar.classList.remove(CLASS_NAME_NAVBAR_LIGHT);
    navbar.classList.add(CLASS_NAME_NAVBAR_DARK);
  }
};
