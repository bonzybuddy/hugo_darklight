import Fuse from "https://cdn.jsdelivr.net/npm/fuse.js@6.4.6/dist/fuse.esm.js";

const fuseOptions = {
  shouldSort: true,
  threshold: 0.1,
  ignoreLocation: true,
  maxPatternLength: 32,
  minMatchCharLength: 3,
  keys: ["title", "author", "principal_actors", "content", "genres", "name"],
};

const CLASS_NAME_SEARCH_INPUT = "site-search";
const CLASS_NAME_SEARCH_RESULTS = "results-wrapper";
const CLASS_NAME_SEARCH_RESULTS_INPUT = "results-search";
const CLASS_NAME_NO_RESULT = "no-result";
const CLASS_NAME_RESULT_ENTRY = "result-entry";
const CLASS_NAME_RESULT = "entries";
const CLASS_NAME_LOW_OPACITY = "low-opacity";
const CLASS_NAME_SHOW = "show";
const MAX_CONTENT_LENGTH = 150;

document.addEventListener("DOMContentLoaded", function () {
  const input = document.querySelector(`.${CLASS_NAME_SEARCH_INPUT}`);
  const resultsWrapper = document.querySelector(
    `.${CLASS_NAME_SEARCH_RESULTS}`
  );
  const resultInput = resultsWrapper.querySelector(
    `.${CLASS_NAME_SEARCH_RESULTS_INPUT}`
  );

  resultInput.addEventListener(
    "input",
    debounce(async function () {
      const results = await executeSearch(resultInput.value);
      render(results);
    }, 250)
  );

  if (resultInput.value !== "") {
    (async () => {
      const results = await executeSearch(resultInput.value);
      render(results);
    })();
  }

  window.addEventListener("keydown", function (e) {
    if (e.ctrlKey && e.key == "x") {
      openSearchBox();
    }
  });

  input.addEventListener("click", openSearchBox);

  const close = resultsWrapper.querySelector(".close");
  close.addEventListener("click", function () {
    closeSearchBox();
  });

  window.addEventListener("keydown", function (event) {
    if (event.key === "Escape") {
      closeSearchBox();
    }
  });
});

async function executeSearch(searchQuery = "") {
  const res = await fetch("/index.json");
  const data = await res.json();
  let result = [];
  try {
    const fuse = new Fuse(data, fuseOptions);
    result = fuse.search(searchQuery);
  } catch (error) {
    console.error(error);
  } finally {
    console.log({ matches: result });
    return result;
  }
}

function openSearchBox() {
  const input = document.querySelector(`.${CLASS_NAME_SEARCH_INPUT}`);
  const resultsWrapper = document.querySelector(
    `.${CLASS_NAME_SEARCH_RESULTS}`
  );
  const resultInput = resultsWrapper.querySelector(
    `.${CLASS_NAME_SEARCH_RESULTS_INPUT}`
  );
  resultsWrapper.classList.add(CLASS_NAME_SHOW);
  input.classList.remove(CLASS_NAME_SHOW);

  document.body.classList.add(CLASS_NAME_LOW_OPACITY);

  setTimeout(() => {
    window.addEventListener("click", closeSearchBoxWhenClickOutside);
    resultInput.focus();
  }, 50);
}

function closeSearchBox() {
  const input = document.querySelector(`.${CLASS_NAME_SEARCH_INPUT}`);
  const resultsWrapper = document.querySelector(
    `.${CLASS_NAME_SEARCH_RESULTS}`
  );

  document.body.classList.remove(CLASS_NAME_LOW_OPACITY);

  resultsWrapper.classList.remove(CLASS_NAME_SHOW);
  input.classList.add(CLASS_NAME_SHOW);
  window.removeEventListener("click", closeSearchBoxWhenClickOutside);
}

function closeSearchBoxWhenClickOutside(event) {
  const resultsWrapper = document.querySelector(
    `.${CLASS_NAME_SEARCH_RESULTS}`
  );
  const input = document.querySelector(`.${CLASS_NAME_SEARCH_INPUT}`);
  let parent = event.target;
  while (parent != document.body) {
    if (parent === resultsWrapper || parent === input) {
      return;
    }
    parent = parent.parentElement;
  }
  closeSearchBox();
}

function render(matches = []) {
  const resultsWrapper = document.querySelector(
    `.${CLASS_NAME_SEARCH_RESULTS}`
  );
  const entries = resultsWrapper.querySelector(`.${CLASS_NAME_RESULT}`);
  entries.innerHTML = "";

  if (matches.length === 0) {
    const template = document.querySelector(`.${CLASS_NAME_NO_RESULT}`);
    const noResult = document.importNode(template.content, true);

    entries.append(noResult);
    return;
  }

  matches
    .map((match) => match.item)
    .forEach((item) => {
      const template = document.querySelector(`.${CLASS_NAME_RESULT_ENTRY}`);
      const entry = document.importNode(template.content, true);
      const a = entry.querySelector("a");
      const dt = entry.querySelector("dt");
      const dd = entry.querySelector("dd");

      a.href = item.permalink;
      a.innerHTML = item.title;
      dd.innerHTML = `${item.content.substring(0, MAX_CONTENT_LENGTH)}...`;
      entries.appendChild(entry);
    });
}

function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
}
