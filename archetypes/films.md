---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
# maj: "08 Fév 2021"
draft: true
author : "Stanley"
principal_actors: "Robert"
genres:
  - Drame
release_year: "2020"
duree : "2h"
cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
#toc: false
---

## 🗣 De quoi ça parle

## 🔎 Comment je l'ai découvert

## 💭 Ce que j'en ai pensé

## 🎯 Qui l'apprécierait

## 🔗 Œuvres reliées
