---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
genres: 
- Roman
author: "John"
fiction: false
release_year: "2001"
duree:
# maj: "08 Fév 2021"
cover: 
  img: /covers/DerekSivers-AnythingYouWant-cover.jpg
  source: https://sive.rs/a/
#toc: false
---

## 🥇🥈🥉 Le livre en 3 phrases

-
-
-

## 🎣 Impressions

## 🔎 Comment je l'ai découvert

## 🎯 Qui devrait le lire

## 🎉️ Ce que le livre a changé chez moi

**Comment ma vie / comportement / idées / pensées ont évolués après la lecture de ce livre.**

-
-
-

## 💬 Mes 3 citations préférées

>

## 📝 Résumé + notes personnelles
